# Secret Story
## La gestions des Secrets
---
# HashiCorp Vault


![https://www.vaultproject.io/](images/vault_logo_300px.png)

---

## Késako
* Vault est un service de gestion de secrets (mots de passe, clés API, jetons, certificats etc...).
* Il fournit une API qui donne accès à des secrets basés sur des politiques. Tout utilisateur de l'API doit s'authentifier et ne voit que les secrets pour lesquels il est autorisé.  
* Vault chiffre les données. Il peut les stocker dans differents backends (fichiers, AWS DynamoDB, Consul, etcd, ZooKeeper etc...).  


---

### Installation
Vault est ecrit en Go.  
Vault n'est qu'un binaire à telécharger [ici](https://www.vaultproject.io/downloads.html) et à mettre dans votre PATH  
Note: On vera plus tard que cela n'a pas que des avantages.

---?code=files/install.sh&lang=bash

---

### Configuration
La configuration se fait dans un langage spécifique HashiCorp le hcl.

---?code=files/config.hcl&lang=vim

---

### Launch
Je recommande l'utilisation de [supervisord](http://supervisord.org/), pour lancer Vault de maniere automatique et pour, entre autre, la gestion des logs.  

---?code=files/launch.sh&lang=bash

---

### Initialisation
La premiere fois, il faut initialiser le backend de stockage. A ce moment Vault génére les clés de "descellement" du coffre,ainsi que le token root.  
/!\ Attention à bien noter ces infos et à les stocker dans un endroit sécurisé. En cas de perte, les secrets seront perdus.

---?code=files/init.sh&lang=vim
Note: Le backend de stockage peut etre file, dyanmodb, postgresql, Consul, etcd, ZooKeeper, Azure, S3 etc...
---

### Seal /  Unseal
Vault n'enregistre jamais de clé dans un emplacement persistant. Le démarrage de Vault nécessite un dévérouillage de la part d'un ou plusieurs operateurs.  
Il est conseillé de désactiver la swap sur les serveur Unix.
Note: vault ne fait que lire les données chiffrées dans le backend de stockage et de les dechiffrer, rien n'est stocké en clair.

---?code=files/unseal.sh&lang=bash
Note: Par default à l'init 5 clé d'unseal necessite 3. possibilité dans generer qu'une (-key-shares=1 -key-threshold=).
---
## Secrets Engine
---
* Composants qui stockent, generent ou chiffrent les données
* Moteur qui stocke et lise uniquement (kv,ssh,...)
* Moteurs qui connectent à des services et generent des identifications dynamiques (AWS, Database,etc... )
* Moteurs qui fournissent le chiffrement (topt, certificats(pki))
* Les sont moteurs sont activés sur un PATH

Note: Les moteurs Secrets sont activés sur un "chemin" dans Vault. Quand une requête arrive à Vault, le routeur achemine automatiquement tout ce qui a un préfixe de route vers le moteur secret. De cette façon, chaque moteur de secrets définit ses propres chemins et propriétés. Pour l'utilisateur, les moteurs secrets se comportent comme un système de fichiers virtuel, prenant en charge des opérations telles que lire, écrire et supprimer.

---
## Auth Methods
Les méthodes d'authentification sont les composants de Vault qui effectuent l'authentification et sont responsables de l'attribution de l'identité et d'un ensemble de stratégies à un utilisateur. On peut utiliser plusieurs methodes d'authentification:  
AWS, GitHub, Approle, Google Cloud, Kubernetes, LDAP, Token etc...
Note: Certaine peuvent etre managé ex: AXS Kubernetes
---
![](images/vault1.svg)
---
## On peut l'utiliser ?
### Demonstration
![](images/demo.jpg)
Note: Demo vault launch, demo postman, demo vault-ui, demo golang

---

## Tips and Tricks
* Unseal  
```bash
    /usr/local/bin/vault unseal -address http://127.0.0.1:8200 $(grep \"Unseal Key 1: \" .secrets.vault|awk -F: '{print substr($2,2)}')
```
* Storage Backend Cluster  
utiliser Consul ou Dynamodb pour le lien de vie cluster (il peut etre different du Storage Backend)
* Attention au WAF  
les instructions "delete" et "put" sont souvent bloquées. On peut utiliser "post" à la place de "put".
