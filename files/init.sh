 /usr/local/bin/vault init -key-shares=1 -key-threshold=1 -address http://127.0.0.1:8200

   Unseal Key 1: E5opjoURwQymZ7/A3qWR5YfZnxDYwMOT6BgxFhT6Zfw=
   Initial Root Token: c8fb40e8-5647-a4d9-48f2-f2c8520a87ac
   
   Vault initialized with 1 keys and a key threshold of 1. Please
   securely distribute the above keys. When the vault is re-sealed,
   restarted, or stopped, you must provide at least 1 of these keys
   to unseal it again.

   Vault does not store the master key. Without at least 1 keys,
   your vault will remain permanently sealed.
