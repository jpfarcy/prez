api_addr = "http://<<VAULT_URI>>"
cluster_addr = "http://<<VAULT_IP>>:8201"
storage "dynamodb" {
  max_parallel = 128
  region = "eu-west-1"
  table = "<<VAULT_DATA>>"
  read_capacity  = 10
  write_capacity = 15
  ha_enabled = "true"
  redirect_addr = "http://<<VAULT_URI>>"
}
listener "tcp" {
	  address     = ":8200"
	  tls_disable = 1
}
disable_mlock = 1
