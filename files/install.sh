 cd /usr/local/bin/
 sudo wget https://releases.hashicorp.com/vault/0.9.4/vault_0.9.4_linux_amd64.zip
 sudo wget https://releases.hashicorp.com/vault/0.9.4/vault_0.9.4_SHA256SUMS
 sha256sum -c vault_0.9.4_SHA256SUMS 2>&1 | grep OK
 sudo unzip vault_0.9.4_linux_amd64.zip
 sudo rm vault_0.9.4_linux_amd64.zip
