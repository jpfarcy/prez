package main

import (
	"encoding/json"
	"fmt"
	"os"
  "github.com/hashicorp/vault/api"
  "log"
)
type Config struct {
		Token     string `json:"token"`
		VaultAddr string `json:"vault_addr"`
		SecretPath string `json:"secret_path"`
		DbUser string `json:"database_user_name"`
		DbPassword string `json:"database_user_password"`
}

func LoadConfiguration(file string) (Config, error) {
	var config Config
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		os.Exit(1)
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)
	return config, err
}

func main() {
	fmt.Println("Load config")
	var config, _ = LoadConfiguration("config.json")

// Initiate API
  var VClient *api.Client
  Vconf := &api.Config{
        Address: config.VaultAddr,
    }

  VClient, _ = api.NewClient(Vconf)
  VClient.SetToken(config.Token)

// Set Secret (map)
  secretData := make(map[string]interface{})
  secretData["DB_USER"] = config.DbUser
  secretData["DB_PASSWORD"] = config.DbPassword

// Write Secret in Vault
  _, err := VClient.Logical().Write(config.SecretPath, secretData)
  if err != nil {
      log.Fatal(err)
  }

// Read Secret in Vault
  secret, _ := VClient.Logical().Read(config.SecretPath)
  //log.Printf("%#v", *secret)
	for key, value := range secret.Data {
    fmt.Println("Key:", key, "Value:", value)
	}

}
